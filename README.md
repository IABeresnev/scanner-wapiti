# Wapiti Scanner

## Nginx
   Используется как прокси-сервер, при деплое использует конфигурацию из папки config/default.conf 
## PHP-FPM
   Используется как обработчик PHP+Wapiti, при деплое использует конфигурацию из папки status/www.conf
## APP-Folder
   Директория с Frontend.
## Monitoring
   Для получения состояния контейнера с Nginx добавлен healthcheck в docker-compose файл.
   Для получения состояния php-fpm настроено получения статуса с web /fpm-status или /fpm-status?(full, json, etc.) и /fpm-ping
