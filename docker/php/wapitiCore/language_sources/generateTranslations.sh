#!/bin/sh
rm ../data/language/de/LC_MESSAGES/wapiti.mo 2> /dev/null
rm ../data/language/fr/LC_MESSAGES/wapiti.mo 2> /dev/null
rm ../data/language/en/LC_MESSAGES/wapiti.mo 2> /dev/null
rm ../data/language/es/LC_MESSAGES/wapiti.mo 2> /dev/null
rm ../data/language/ms/LC_MESSAGES/wapiti.mo 2> /dev/null
rm ../data/language/pt/LC_MESSAGES/wapiti.mo 2> /dev/null
rm ../data/language/zh/LC_MESSAGES/wapiti.mo 2> /dev/null
rm ../data/language/ru/LC_MESSAGES/wapiti.mo 2> /dev/null

echo "German"
msgfmt de.po -o ../language/de/LC_MESSAGES/wapiti.mo
echo "English"
msgfmt en.po -o ../language/en/LC_MESSAGES/wapiti.mo
echo "Spanish"
msgfmt es.po -o ../language/es/LC_MESSAGES/wapiti.mo
echo "French"
msgfmt fr.po -o ../language/fr/LC_MESSAGES/wapiti.mo
echo "Malay"
msgfmt ms.po -o ../language/ms/LC_MESSAGES/wapiti.mo
echo "Portuguese"
msgfmt pt.po -o ../language/pt/LC_MESSAGES/wapiti.mo
echo "Chinese"
msgfmt zh.po -o ../language/zh/LC_MESSAGES/wapiti.mo
echo "Russian"
msgfmt ru.po -o ../language/ru/LC_MESSAGES/wapiti.mo
